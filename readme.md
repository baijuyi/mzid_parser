

#### Table A

```python
# protein (info)
dBSequence_ref_accession_df = db_sequence_df_def(root, name_space)
```

|      | dBSequence_ref           | accession         |
| ---- | ------------------------ | ----------------- |
| 1    | DBSeq_IPI:CON_00009867.3 | SWISS-PROT:P13647 |
| 2    | DBSeq_IPI:CON_00019359.3 | SWISS-PROT:P35527 |
| 3    | DBSeq_IPI:CON_00021304.1 | SWISS-PROT:P35908 |
| 4    | DBSeq_IPI:CON_00112947.1 | SWISS-PROT:P19001 |

#### Table B

```python
# spectrum and protein group
group_dBSequence_spectrum_df = proteinAmbiguityGroup_df_def(root, name_space)
```

|      | protein_group      | dBSequence_ref    | spectrum_identification  |
| ---- | ------------------ | ----------------- | ------------------------ |
| 1    | IPI:CON_00220327.3 | SWISS-PROT:P04264 | DBSeq_IPI:CON_00220327.3 |
| 2    | IPI:CON_00220327.3 | SWISS-PROT:P04264 | DBSeq_IPI:CON_00220327.3 |
| 3    | IPI:CON_00220327.3 | SWISS-PROT:P04264 | DBSeq_IPI:CON_00220327.3 |
| 4    | IPI:CON_00220327.3 | SWISS-PROT:P04264 | DBSeq_IPI:CON_00220327.3 |

#### Table C

```python
sample_spectrum_df = sample_spectrum_df_def(root, name_space)
```

|      | sample                         | spectrum_identification |
| ---- | ------------------------------ | ----------------------- |
| 1    | MSB29562CplusBand_10 (F127143) | Spec_60708_SFLDK        |
| 2    | MSB29562CplusBand_10 (F127143) | Spec_60709_KIIIK        |
| 3    | MSB29562CplusBand_10 (F127143) | Spec_60711_FGLQR        |
| 4    | MSB29562CplusBand_10 (F127143) | Spec_60712_AGLLEK       |



#### Table D: C join B, on ‘spectrum_identification’

```python
sample_spectrum_group_dBSequence_df = pd.merge(sample_spectrum_df, \
                                                group_dBSequence_spectrum_df, \
                                                on='spectrum_identification', \
                                                how='inner')
```

|    |                       sample   | spectrum_identification |          protein_group |              dBSequence_ref |
|--- |          ----                  |           ----          |            ----        |                 ---         |
|1   |  MSB29562CplusBand_10 (F127143)|        Spec_60708_SFLDK |  sp\|B9EJ86\|OSBL8_MOUSE | DBSeq_sp\|B9EJ86|OSBL8_MOUSE |
|2   |  MSB29562CplusBand_10 (F127143)|        Spec_60709_KIIIK |   sp\|P13705\|MSH3_MOUSE |  DBSeq_sp\|P13705|MSH3_MOUSE |
|3   |  MSB29562CplusBand_10 (F127143)|        Spec_60711_FGLQR |   sp\|Q19LI2\|A1BG_MOUSE |  DBSeq_sp\|Q19LI2|A1BG_MOUSE |
|4   |  MSB29562CplusBand_10 (F127143)|       Spec_60712_AGLLEK |  sp\|Q61176\|ARGI1_MOUSE | DBSeq_sp\|Q61176|ARGI1_MOUSE |


#### Table E: C join A, on ’dBSequence_ref'

```python
sample_spectrum_group_peptide_accession_df = 	pd.merge(sample_spectrum_group_dBSequence_df, \
					dBSequence_ref_accession_df, \
          on='dBSequence_ref', \
          how='inner')
```

|     |                         sample | spectrum_identification |          protein_group |               dBSequence_ref |              accession |
| --- |                ---             |              ---        |              ---       |                 ---          |          ---           |
| 1   | MSB29562CplusBand_10 (F127143) |   Spec_60950_FSIASN+1YR |  sp\|P07759\|SPA3K_MOUSE |  DBSeq_sp\|P07759|SPA3K_MOUSE |  sp\|P07759\|SPA3K_MOUSE |
| 2   | MSB29562CplusBand_10 (F127143) |   Spec_61175_KLSVSQVVHK |  sp\|P07759\|SPA3K_MOUSE |  DBSeq_sp\|P07759|SPA3K_MOUSE |  sp\|P07759\|SPA3K_MOUSE |
| 3   | MSB29562CplusBand_10 (F127143) |   Spec_61176_KLSVSQVVHK |  sp\|P07759\|SPA3K_MOUSE |  DBSeq_sp\|P07759|SPA3K_MOUSE |  sp\|P07759\|SPA3K_MOUSE |
| 4   | MSB29562CplusBand_10 (F127143) |   Spec_61212_TMEEILEGLK |  sp\|P07759\|SPA3K_MOUSE |  DBSeq_sp\|P07759|SPA3K_MOUSE |  sp\|P07759\|SPA3K_MOUSE |


#### Table F: group E by ['protein_group', 'sample’], and count 'spectrum_identification'

```python
group_sample_count_df = sample_spectrum_group_peptide_accession_df \
                        .groupby(['protein_group', 'sample'], \
                        as_index=False) \
                        .agg({'spectrum_identification': pd.Series.nunique})
```

|     |                       protein_group   |                        sample   | spectrum_identification |
| --- |            ---                        |                ---              |           ---           |
| 1   |  IPI:CON_00009867.3\|SWISS-PROT:P13647 |  MSB29562CplusBand_10 (F127143) |          47             |
| 2   |  IPI:CON_00019359.3\|SWISS-PROT:P35527 |  MSB29562CplusBand_10 (F127143) |          44             |
| 3   |  IPI:CON_00021304.1\|SWISS-PROT:P35908 |  MSB29562CplusBand_10 (F127143) |          50             |
| 4   |  IPI:CON_00112947.1\|SWISS-PROT:P19001 |  MSB29562CplusBand_10 (F127143) |           5             |


#### Table G:group E by ‘protein_group’, and merge all accseeion into a list

```python
# group_by `protein_group`, merge all proteins (name) into a string under one group
group_accessions_df = sample_spectrum_group_peptide_accession_df \
                        .groupby('protein_group', as_index=False) \
                        .agg({'accession': lambda x: x.unique().tolist()})
```

|      | protein_group                         | accession                               |
| ---- | ------------------------------------- | --------------------------------------- |
| 1    | IPI:CON_00009867.3\|SWISS-PROT:P13647 | [IPI:CON_00009867.3\|SWISS-PROT:P13647] |
| 2    | IPI:CON_00019359.3\|SWISS-PROT:P35527 | [IPI:CON_00019359.3\|SWISS-PROT:P35527] |
| 3    | IPI:CON_00021304.1\|SWISS-PROT:P35908 | [accession1]                            |
| 4    | IPI:CON_00112947.1\|SWISS-PROT:P19001 | [accession1, accession2, accession3]    |



#### Table H: F join G, on ‘protein_group’ 

```python
return_data = pd.merge(group_sample_count_df, group_accessions_df, on='protein_group', how='inner')
```

|      | protein_group                         | sample                         | spectrum <br>identification | accession                                            |
| ---- | ------------------------------------- | ------------------------------ | --------------------------- | ---------------------------------------------------- |
| 1    | IPI:CON_00009867.3\|SWISS-PROT:P13647 | MSB29562CplusBand_10 (F127143) | 47                          | [IPI:CON_00009867.3\|SWISS-PROT:P13647]              |
| 2    | IPI:CON_00019359.3\|SWISS-PROT:P35527 | MSB29562CplusBand_10 (F127143) | 44                          | [IPI:CON_00019359.3\|SWISS-PROT:P35527]              |
| 3    | IPI:CON_00021304.1\|SWISS-PROT:P35908 | MSB29562CplusBand_10 (F127143) | 50                          | [IPI:CON_00021304.1\|SWISS-PROT:P35908]              |
| 4    | IPI:CON_00112947.1\|SWISS-PROT:P19001 | MSB29562CplusBand_10 (F127143) | 5                           | [sp\|P19001\|K1C19_MOUSE, IPI:CON_00112947.1\|SWI... |